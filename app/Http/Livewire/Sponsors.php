<?php

namespace App\Http\Livewire;
use App\Models\Sponsor;
use Livewire\Component;

class Sponsors extends Component
{
    public $sponsors;

    public function render()
    {
        // $this->sponsors = Sponsor::limit(15)->get();
        $this->sponsors = Sponsor::orderBy('created_at', 'DESC')->limit(30)->get();

        return view('sponsors');
    }

    public function search(Request $request)
    {
    $query = $request->input('query');
    
    // Assuming your Sponsor model is used to interact with the sponsors collection
    $sponsors = Sponsor::where('phone_number', 'LIKE', "%{$query}%")
                        ->orWhere('referral_code', 'LIKE', "%{$query}%")
                        ->get();

    return view('sponsors', compact('sponsors','sponsors'));
    }
}
