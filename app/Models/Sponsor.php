<?php
namespace App\Models;
use MongoDB\Laravel\Eloquent\Model;

class Sponsor extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'users';
    protected $fillable = ['phone_number', 'created_at', 'has_paid_license_fee', 'referral_code', 'referring_merchant', 'merchant_number'];
}



